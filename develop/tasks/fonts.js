// Gulp

    var gulp            = require('gulp');
    var runSequence     = require('run-sequence');
    var $               = require('gulp-load-plugins')();

// Modules

    // var fontmin         = require('gulp-fontmin');

// Task

    // Making Fonts

        gulp.task('fonts-ttf', function () {
            return gulp.src('./public/css/fonts/*.ttf')
                .pipe($.plumber({errorHandler: $.notify.onError("(•): <%= error.message %>")}))
                .pipe($.fontmin({
                    fontPath: '../css/fonts/_fontname_/'
                }))
                .pipe(gulp.dest('./develop/minified/fonts/'))
        });

        gulp.task('fonts-css', function () {
            return gulp.src('./develop/minified/fonts/*.css')
                .pipe($.plumber({errorHandler: $.notify.onError("(•): <%= error.message %>")}))
                .pipe($.concat('typography.css'))
                .pipe(gulp.dest('./develop/minified/fonts/'))
                .pipe($.notify({
                    onLast: true,
                    message: function () {
                        return ' • typography.css • ready! '
                    }
                })
             );
        });

gulp.task('fonts', function(callback) {
    runSequence(
        'fonts-ttf',
        'fonts-css',
    callback);
});