// Gulp

	var gulp 			= require('gulp');
	var $ 				= require('gulp-load-plugins')();
	var fs 				= require('fs');

// Modules

// var realFavicon 		= require(gulp-real-favicon);

	// Creating Favicon

		var FAVICON_DATA_FILE = 'faviconData.json';

		gulp.task('favicon:create', function(done) {
			$.realFavicon.generateFavicon({
				masterPicture: './source/image/favicon/favicon.png',
				dest: './public/img/favicon',
				iconsPath: '/',
				design: {
					ios: {
						pictureAspect: 'backgroundAndMargin',
						backgroundColor: '#ffffff',
						margin: '53%'
					},
					desktopBrowser: {},
					windows: {
						pictureAspect: 'noChange',
						backgroundColor: '#da532c',
						onConflict: 'override'
					},
					androidChrome: {
						pictureAspect: 'noChange',
						themeColor: '#ffffff',
						manifest: {
							name: 'AppName',
							display: 'browser',
							orientation: 'notSet',
							onConflict: 'override'
						}
					},
					safariPinnedTab: {
						pictureAspect: 'silhouette',
						themeColor: '#5bbad5'
					}
				},
				settings: {
					compression: 1,
					scalingAlgorithm: 'Mitchell',
					errorOnImageTooSmall: false
				},
				markupFile: FAVICON_DATA_FILE
			}, function() {
				done();
			});
		});

	// Injecting Favicon in HTML

		gulp.task('favicon:inject', function() {
			gulp.src([ 'TODO: List of the HTML files where to inject favicon markups' ])
				.pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
				.pipe(gulp.dest('TODO: Path to the directory where to store the HTML files'));
		});

	// Favicon List Update

		gulp.task('favicon:uptade', function(done) {
			var currentVersion = JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).version;
			$.realFavicon.checkForUpdates(currentVersion, function(err) {
				if (err) {
					throw err;
				}
			});
		});

gulp.task('fav', ['favicon:create']);