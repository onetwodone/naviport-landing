// Gulp

	var gulp 			= require('gulp');
	var $				= require('gulp-load-plugins')();

// Modules

	// var jshint 			= require('gulp-jshint');
	// var jscs 			= require('gulp-jscs');

	// Linting (JS)

		var js_list = [
			'./public/js/**/*.js'
		];

		gulp.task('lint:js', function () {
	        return gulp.src(js_list)
		        .pipe($.jscs())
		        .pipe($.jshint())
		        .pipe($.jshint.reporter('jshint-stylish'))
		        .pipe($.jshint.reporter('fail'));
	    });
 
gulp.task('lint', ['lint:js', 'lint:scss']);