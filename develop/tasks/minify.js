// Gulp

    var gulp            = require('gulp');
    var path            = require('path');
    var $               = require('gulp-load-plugins')();
    var fs              = require('fs');
    var merge           = require('merge-stream');
    var runSequence     = require('run-sequence');

// Modules

    // var size            = require('gulp-size');
    // var rename          = require('gulp-rename');
    // var minifyhtml      = require('gulp-minify-html');
    // var minifycss       = require('gulp-minify-css');
    // var uglify          = require('gulp-uglify');
    // var imagemin        = require('gulp-imagemin');

    // var plumber          = require('gulp-plumber');
    // var notify           = require("gulp-notify");

// Options

    var js_min_files = [

        // Compile

        './public/js/*.js',
        './public/js/libs/*.js'

        // Ignoring

        // '!./public/js/libs/*.js',

    ];

// Tasks

    // Minify HTML Task

        gulp.task('mhtml', function () {

            var beforeSize = $.size();
            var afterSize = $.size();

            return gulp.src('./public/**/*.html')

                .pipe($.plumber({errorHandler: $.notify.onError("(•): <%= error.message %>")}))
                .pipe(beforeSize)
                .pipe($.minifyHtml({
                    conditionals: true,
                    spare: true
                }))
                .pipe(afterSize)
                .pipe(gulp.dest('./develop/build/html/'))
                .pipe($.notify({
                    onLast: true,
                    message: function () {
                        return ' • HTML files • ' + beforeSize.prettySize + ' → ' + afterSize.prettySize;
                    }
                })
            );
        });

    // Minify CSS Task

        gulp.task('mcss', function () {

            var beforeSize = $.size();
            var afterSize = $.size();

            return gulp.src('./public/css/*.css')

                .pipe($.plumber({errorHandler: $.notify.onError("(•): <%= error.message %>")}))
                .pipe(beforeSize)
                .pipe($.minifyCss())
                .pipe($.rename({
                    suffix: ".min",
                }))
                .pipe(afterSize)
                .pipe(gulp.dest('./develop/build/css/'))
                .pipe($.notify({
                    onLast: true,
                    message: function () {
                        return ' • CSS files • ' + beforeSize.prettySize + ' → ' + afterSize.prettySize;
                    }
                })
            );
        });

    // Minify JS Task

        gulp.task('mjs', function () {

            var beforeSize = $.size();
            var afterSize = $.size();

            return gulp.src(js_min_files)
                .pipe($.plumber({errorHandler: $.notify.onError("(•): <%= error.message %>")}))
                .pipe(beforeSize)
                .pipe($.uglify())
                .pipe($.rename({
                    suffix: ".min",
                }))
                .pipe(afterSize)
                .pipe(gulp.dest('./develop/build/js/'))
                .pipe($.notify({
                    onLast: true,
                    message: function () {
                        return ' • JS files • ' + beforeSize.prettySize + ' → ' + afterSize.prettySize;
                    }
                })
            );
        });

    // Minify Images Task

        gulp.task('mimg', function () {

            var beforeSize = $.size();
            var afterSize = $.size();

            return gulp.src('./public/img/**/*')
                .pipe($.plumber({errorHandler: $.notify.onError("(•): <%= error.message %>")}))
                .pipe(beforeSize)
                .pipe($.imagemin({
                    progressive: true,
                    interlaced: true
                }))
                .pipe(afterSize)
                .pipe(gulp.dest('./develop/build/img/'))
                .pipe($.notify({
                    onLast: true,
                    message: function () {
                        return ' • IMG files • ' + beforeSize.prettySize + ' → ' + afterSize.prettySize;
                    }
                })
            );
        });

gulp.task('m', function(callback) {
    runSequence(
        'mhtml',
        'mcss',
        'mjs',
        'mimg',
    callback);
});
