// Gulp

    var gulp            = require('gulp');
    var path            = require('path');
    var fs              = require('fs');
    var merge           = require('merge-stream');

    var $               = require('gulp-load-plugins')();

// Modules

    // var notify          = require('gulp-notify');
    // var plumber         = require('gulp-plumber');
    // var jade            = require('gulp-jade');
    // var sass            = require('gulp-sass');
    // var autoprefixer    = require('gulp-autoprefixer');
    // var concat          = require('gulp-concat');
    // var csscomb         = require('gulp-csscomb');
    // var sourcemaps      = require('gulp-sourcemaps');

// Options

    var prefix_list = [
        'last 15 version',
        'ie >= 9',
        'ie_mob >= 10',
        'ff >= 30',
        'chrome >= 34',
        'safari >= 7',
        'opera >= 23',
        'ios >= 7',
        'android >= 4.4',
        'bb >= 10'
    ];

    var jade_folders = [

        // Compile

        './source/jade/**/*.jade',

        // Ignoring

        '!./source/jade/include/**/*.jade',
        '!./source/jade/mixins/*.jade',
        '!./source/jade/template/*.jade',
    ];

// Watch Task

    gulp.task('watch',function() {

        gulp.watch(path.join('./source/sass/**/*.scss'), ['sass']);
        gulp.watch('./source/jade/**/*.jade', ['jade']);

    });

// JADE compile

    gulp.task('jade', function () {

        return gulp.src(jade_folders)
            .pipe($.plumber({ errorHandler: $.notify.onError('(error): <%= error.message %>') }))
            .pipe($.jade({pretty: true}))
            .pipe(gulp.dest('./public'));
    });

// SASS subdirs compile

    gulp.task('sass', function () {

        var merged = merge();

        fs.readdirSync('./source/sass/').filter(function(subdir) {
            return fs.statSync(path.join('./source/sass/', subdir)).isDirectory();
        }).forEach(function(subdir) {
            merged.add(gulp.src(path.join('./source/sass/', subdir, '*.scss'))
                .pipe($.plumber({ errorHandler: $.notify.onError('<%= error.message %>') }))
                .pipe($.sass())
                .pipe($.autoprefixer({
                    browsers: prefix_list,
                    cascade: false
                }))
                .pipe($.csscomb('./csscomb.json'))
                .pipe($.concat(subdir + '.css'))
                .pipe(gulp.dest('./public/css')));
        });
        return merged;
    });

gulp.task('default', ['watch']);