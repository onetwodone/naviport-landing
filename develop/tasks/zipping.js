// Gulp

    var gulp            = require('gulp');
    var $               = require('gulp-load-plugins')();

// Modules

    // var zip          = gulp-zip;
    // var size         = gulp-size;

// Options

    var project = {
        name: 'advanced'
    };

    var beforeSize = $.size();
    var afterSize = $.size();

// Get Date for Archive

    var getStamp = function() {

        var myDate = new Date();

        var myYear = myDate.getFullYear().toString();
        var myMonth = ('0' + (myDate.getMonth() + 1)).slice(-2);
        var myDay = ('0' + myDate.getDate()).slice(-2);
        var myHours = myDate.getHours().toString();
        var myMinuts = myDate.getMinutes().toString();

        var myFullDate = myYear + myMonth + myDay + '_' + myHours + myMinuts;

        return myFullDate;
    };

// Zipping Tasks

    // Zipping for Public

        gulp.task('zip:pub', function () {

            const zipName = project.name + '_' + getStamp() + '.zip';

            return gulp.src('./public/**/*')
                .pipe(beforeSize)
                .pipe($.zip(zipName))
                .pipe(afterSize)
                .pipe(gulp.dest('./'))
                .pipe($.notify({
                    onLast: true,
                    message: function () {
                        return ' • '+ zipName +' • ' + afterSize.prettySize;
                    }
                })
            );
        });

     // Zipping for Developing

        // ToDo

gulp.task('zip', ['zip:pub']);