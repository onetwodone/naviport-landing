// Gulp

    var gulp            = require('gulp');
    var merge           = require('merge-stream');

    var $               = require('gulp-load-plugins')();


// Modules

    // var sprites         = require('gulp.spritesmith');

// Tasks

    // Making Sprites

        gulp.task('sprites:create', function () {
            var spriteData =
                gulp.src('./public/img/icons/*.png')
                    .pipe($.spritesmith({
                        imgName: 'sprite.png',
                        cssName: 'sprite.css'
                    })
                );
                return spriteData.pipe(gulp.dest('./develop/build/sprites/'));
        });

gulp.task('sprites', ['sprites:create']);