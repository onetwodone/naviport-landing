# html5-advanced
A professional front-end started template

## Structure derictories and files

```
ROOT
│
├── develop/                   # Develop directory
│   │
│   ├── build/                 # Output derectory for tasks
│   ├── fonts/                 # Fonts library
│   ├── other/                 # 
│   ├── tasks/                 # Tasks library (Gulp)
│   └── test/                  # Tests library (Travis)
│
├── public/                    # Public directory
│   │
│   ├── css/                   # Compiled CSS
│   │   └── libs/              # Compiled Fonts
│   ├── img/                   # Compiled Images
│   ├── js/                    # Compiled JS
│   │   ├── libs/              # JS Libraries
│   │   └── ...                # 
│   └── ...                    # Compiled HTML
│
├── source/                    # Source directory
│   │
│   ├── image/                 # 
│   │   └── favicon/           # 
│   ├── jade/                  # JADE templates
│   │   ├── include/           # 
│   │   ├── mixins/            # 
│   │   ├── template/          # 
│   │   └── ...                # 
│   └── sass/                  # SASS templates
│       ├── defaults/          # 
│       ├── layouts/           # 
│       ├── pages/             # 
│       ├── vendors/           # 
│       └── _config.scss       # SASS variables and mixins
│    
│                              # ROOT directory
│    
├── .csscomb.json              #
├── .eslintrc                  # 
├── .editorconfig              # 
├── .gitignore                 # 
├── package.json               # Dependencies
└── readme.md                  # Readme 
```

## jQuery plugins library

Featherlight    http://github.com/noelboss/featherlight/

Slick           http://kenwheeler.github.io/slick/

Masonry         http://desandro.github.io/masonry/demos/fluid.html

LazyLoad        http://www.appelsiini.net/projects/lazyload

Equalheight     http://github.com/JorenVanHee/EqualHeight .js

CountUp         http://inorganik.github.io/countUp.js/

MaskPlugin      http://igorescobar.github.io/jQuery-Mask-Plugin/

Masonry         http://github.com/desandro/masonry

Mousewheel      http://github.com/jquery/jquery-mousewheel

Scrollbar       http://github.com/gromo/jquery.scrollbar/

Waypoints       http://github.com/imakewebthings/waypoints

## TODO:

Миксин вывода блоков
SASS миксины
Перенос тасков из предидущего шаблона

Сделать хэдер и футер миксинами с возможностью кастомизации (тостер)
инициализация с нуля
отредактировать зависимости
вставить css физерлайта

Стартовый шаблон для лендинга
Стартовый шаблон для магазина

Добавить дефолтные сскриптover menu
Добавить прокрутку в верх
 
Добавить струтктуру секций

Добавить скрипт подстветке номеров текстовых полей

Прикрутить UNCSS

Jade снипет поиска
Jade снипет слайдера
Вставить иморты конфига

------
explore

https://mixitup.kunkalabs.com/
https://github.com/Prinzhorn/skrollr
http://fotorama.io/#birdwatcher__fdb7745a-78c9-418e-a853-5e95d8d2069d
http://www.sitehere.ru/adaptivnaya-navigaciya-dlya-menyu
http://www.fixedgroup.com/administrator/media/home/home_cases_analytics_1437457871.jpg
http://www.fubiz.net/
