// DOM Ready

$(document).on('click', '.garantie-list .s6', function () {
		var data = $(this).attr('to-slide');
		$(this).addClass('active').siblings().removeClass('active');
		$('.garantie__slider').find('.to-slide--n' + data).addClass('active').siblings().removeClass('active');
});


$(document).on('click', '.regform__tab', function () {
	$(this).addClass('active').siblings().removeClass('active');
});

$(document).on('click', '.regform__tab--tab1', function () {
	$('.regform__content--tab1').addClass('active').siblings().removeClass('active');
});

$(document).on('click', '.regform__tab--tab2', function () {
	$('.regform__content--tab2').addClass('active').siblings().removeClass('active');
});


(function($) {

	var allPanels = $('.accordion > .accordion__item > dd').hide();

	$('.accordion > .accordion__item > dt > a').click(function() {

		$this = $(this);
		$target = $this.parent().next();

		if(!$target.parent().hasClass('active')) {
			allPanels.slideUp().parent().removeClass('active');
			$target.slideDown().parent().addClass('active');
		}

		return false;
	});

})(jQuery)


$(document).ready(function() {

	var activeDiv = 1;
	showDiv(activeDiv);

	var timer = setInterval(changeDiv, 6000);

	function changeDiv() {
		activeDiv++;
		if (activeDiv == 5) {
			activeDiv = 1;
		}
		showDiv(activeDiv);
	}

	function showDiv(num) {
		$('.to-slide').siblings().removeClass('active');
		$('.to-slide--n' + num).addClass('active').siblings().removeClass('active');
		$('.garantie-list .n' + num).addClass('active').siblings().removeClass('active');
	}

	// Slick Slider

		if ($('.feedback__list').length) {
			$('.feedback__list').slick({
				infinite: true,
				arrows: true,
				speed: 500,
				autoplaySpeed: 10000,
				slidesToShow: 2,
				autoplay: true,
				easing: 'swing'
			});
		};

	// Equal Columns

		if ($('.feedback__list-b').length) {
			$(function() {
				$('.feedback__list-b').matchHeight();
			});
		 };

	// Custom Scrollbar

 		//   $('.scrollbar').scrollbar();

	// Smooth Scrolling

		try {
			$.browserSelector();
			if($("html").hasClass("chrome")) {
				$.smoothScroll();
			}
		} catch(err) {};

});

	// Lazyload

		// if ($('.lazy').length) {

		// 	$("img.lazy").lazyload({
		//     	effect : "fadeIn"
		//     });
		// };

	// Tabs

		// if ($('.tabs').length) {
		// 	$('.tabs').tabs();
		// };

	// Hover

		// $('.link').hover(
		// 	function() {
		// 		$(this).addClass('hover');
		// 	},
		// 	function() {
		// 		$(this).removeClass('hover');
		// 	}
		// );

	// Sicky Navigation

		// if (!!$('.navigation').offset()) {
		// 	var stickyTop = $('.navigation').offset().top;
		// 	$(window).scroll(function(){
		// 		var windowTop = $(window).scrollTop();
		// 		if (stickyTop < windowTop){
		// 			$('.navigation').addClass('fixed');
		// 		}
		// 		else {
		// 			$('.navigation').removeClass('fixed');
		// 		}
		// 	});
		// }

	// AJAX form

		// $("#callback").submit(function() {

		// 	$.ajax({
		// 		type: "POST",
		// 		url: "mail.php",
		// 		data: $(this).serialize()
		// 	})

		// 	.done(function() {
		// 		$(".callback__submit").val('Отправлено!');
		// 		console.log("done");
		// 	})
		// 	.fail(function() {console.log("Sending mail Error");})
		// 	.always(function() {console.log("Sending mail Complete");});

		//  	return false;

		// });

	// To top

		// $('.top').click(function (e) {
		// 	e.preventDefault();
		// 	$('html, body').animate({scrollTop: 0}, 800);
		// });

	// Same Height

		// var $rows = $('.same-height-columns');

		// $rows.each(function () {
		// 	$(this).find('.column').height($(this).height());
		// });

	// Find elements by text
		// var search = $('#search').val();
		// $('div:not(:contains("' + search + '"))').hide();

// Hash Navigation

	// $(document).on("click", ".navigation__list a" function () {

	// 	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') || location.hostname == this.hostname) {
	// 		var target = $(this.hash);
	// 		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	// 		if (target.length) {
	// 			$('html,body').stop().animate({
	// 				scrollTop: target.offset().top - 80
	// 			}, 600);
	// 			return false;
	// 		}
	// 	}

	// });

// Listener

	// $(document).on("click", ".link", function (e) {
	// 	e.preventDefault();
	// });

// Drag object fix

	$("img, a").on("dragstart", function(e) {
		e.preventDefault();
	});

// Console test

	$(document).ready(function(){
		console.log('js/jquery.functions.js was connected!');
	});

	if (window.jQuery) {
		var jQueryVersion = jQuery.fn.jquery
		console.log('js/jquery.min.js ('+ jQueryVersion + ') was connected!');
	};
